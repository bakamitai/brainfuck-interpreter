use std::iter::Peekable;
use std::str::Chars;

#[derive(Debug)]
enum Instruction {
    IncrementPointer(usize),
    DecrementPointer(usize),
    IncrementByte(u8),
    DecrementByte(u8),
    OutputByte,
    InputByte,
    JumpForward(usize),
    JumpBackward(usize),
    PrintDebugState,
}

const UNCHANGED: u8 = 0;
const NEGATIVE_ONE: u8 = 1;
const ZERO: u8 = 2;

fn print_debug_state(
    program: &Vec<Instruction>,
    program_pointer: usize,
    memory_pointer: usize,
    memory_value: u8,
) {
    let mut depth = 0;

    for (index, instruction) in program.iter().enumerate() {
        match instruction {
            Instruction::JumpForward(_) => {
                for _ in 0..depth {
                    print!("    ");
                }
                print!("{:?}", instruction);
                depth += 1;
            }
            Instruction::JumpBackward(_) => {
                depth -= 1;
                for _ in 0..depth {
                    print!("    ");
                }
                print!("{:?}", instruction);
            }
            _ => {
                for _ in 0..depth {
                    print!("    ");
                }
                print!("{:?}", instruction);
            }
        }
        if index == program_pointer {
            print!(
                " <-- Current instruction\nMemory pointer = {}, memory value = {}\n",
                memory_pointer, memory_value
            );
        }
        print!("\n");
    }
}

fn count_repeats(stream: &mut Peekable<Chars<'_>>, character: char) -> usize {
    let mut amount = 1;
    while let Some(c) = stream.peek() {
        if *c == character {
            amount += 1;
        } else if *c == '<'
            || *c == '>'
            || *c == '+'
            || *c == '-'
            || *c == '.'
            || *c == ','
            || *c == '['
            || *c == ']'
            || *c == '#'
        {
            break;
        }
        stream.next();
    }
    amount
}

fn main() -> std::io::Result<()> {
    let mut debug = false;
    let mut input_after_eof = UNCHANGED;
    let mut filename = String::new();
    let mut input = String::new();
    let mut input_index = 0;
    let mut args = std::env::args().skip(1);

    while let Some(s) = args.next() {
        if s == "--debug" {
            debug = true;
        } else if s == "--input" {
            if let Some(s) = args.next() {
                if s == "-1" {
                    input_after_eof = NEGATIVE_ONE;
                } else if s == "0" {
                    input_after_eof = ZERO;
                } else {
                    eprintln!("Invalid value for \"--input\" switch. Expected \"-1\" or \"0\"");
                    std::process::exit(1);
                }
            } else {
                eprintln!("Failed to provide argument for \"--input\" switch. Expected \"-1\" or \"0\"");
                std::process::exit(1);
            }
        } else {
            filename = s;
            if let Some(s) = args.next() {
                input = s;
            }
        }
    }

    let input = input.as_bytes();

    let mut memory: [u8; 30_000] = [0; 30_000];
    let mut data_pointer: usize = 0;
    let mut program_pointer: usize = 0;
    let mut program: Vec<Instruction> = Vec::with_capacity(256);
    let mut output = String::new();


    let code = std::fs::read_to_string(filename)?;
    let mut character_stream = code.chars().peekable();

    while let Some(c) = character_stream.next() {
        match c {
            '>' => program.push(Instruction::IncrementPointer(count_repeats(
                &mut character_stream,
                '>',
            ))),

            '<' => program.push(Instruction::DecrementPointer(count_repeats(
                &mut character_stream,
                '<',
            ))),

            '+' => program.push(Instruction::IncrementByte(
                count_repeats(&mut character_stream, '+') as u8,
            )),

            '-' => program.push(Instruction::DecrementByte(
                count_repeats(&mut character_stream, '-') as u8,
            )),

            '.' => program.push(Instruction::OutputByte),
            ',' => program.push(Instruction::InputByte),

            '[' => program.push(Instruction::JumpForward(0)),
            ']' => {
                let mut index = program.len() as i32 - 1;
                assert!(index > 0);

                let mut stack = 1;
                let mut match_found = false;
                while index >= 0 {
                    let usize_index = index as usize;
                    if let Instruction::JumpForward(_) = program[usize_index] {
                        stack -= 1;
                    } else if let Instruction::JumpBackward(_) = program[usize_index] {
                        stack += 1;
                    }

                    if stack == 0 {
                        program.push(Instruction::JumpBackward(usize_index));
                        program[usize_index] = Instruction::JumpForward(program.len() - 1);
                        match_found = true;
                        break;
                    }
                    index -= 1;
                }
                assert!(match_found);
            }
            '#' => if debug { program.push(Instruction::PrintDebugState) },
            _ => (),
        }
    }

    while program_pointer < program.len() {
        match program[program_pointer] {
            Instruction::IncrementPointer(n) => data_pointer += n,
            Instruction::DecrementPointer(n) => data_pointer -= n,
            Instruction::IncrementByte(n) => {
                memory[data_pointer] = memory[data_pointer].overflowing_add(n).0
            }
            Instruction::DecrementByte(n) => {
                memory[data_pointer] = memory[data_pointer].overflowing_sub(n).0
            }
            Instruction::OutputByte => output.push(memory[data_pointer] as char),
            Instruction::InputByte => {
                if input_index < input.len() {
                    memory[data_pointer] = input[input_index];
                    input_index += 1;
                } else if input_after_eof == ZERO {
                    memory[data_pointer] = 0;
                } else if input_after_eof == NEGATIVE_ONE {
                    memory[data_pointer] = 255; // Equivalent to -1 in 2's complement
                }
                // If input_after_eof == UNCHANGED, we do nothing
            }
            Instruction::JumpForward(n) => {
                if memory[data_pointer] == 0 {
                    program_pointer = n;
                }
            }
            Instruction::JumpBackward(n) => {
                if memory[data_pointer] != 0 {
                    program_pointer = n;
                }
            }
            Instruction::PrintDebugState => print_debug_state(
                &program,
                program_pointer,
                data_pointer,
                memory[data_pointer],
            ),
        }
        program_pointer += 1;
    }

    println!("{}", output);

    Ok(())
}
